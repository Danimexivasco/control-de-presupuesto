import React, { useState } from 'react';
import Error from './Error';
import shortid from 'shortid';
import PropTypes from 'prop-types';


const Formulario = ({guardarGasto,guardarCrearGasto}) => {

    // Creamos el State de gasto
    const [nombre, guardarNombre] = useState('');

    // Creamos el State de cantidad
    const [cantidad, guardarCantidad] = useState(0);

    // State de error
    const [error, guardarError] = useState(false)

    // Cuando se realiza el submit (agrega un gasto)
    const agregarGasto = e => {
        e.preventDefault()

        // Validar
        if (nombre.trim() === '' || cantidad < 1 || isNaN(cantidad)) {
            guardarError(true);
            return;
        }
        guardarError(false);
        // Construimos el gasto (Obj)
        const gasto={
            nombre,
            cantidad,
            id: shortid.generate()
        }

        console.log(gasto)


        // Pasamos el gasto al componente principal
            // agregarNuevoGasto(gasto)
            guardarGasto(gasto)
            guardarCrearGasto(true)

        // Reseteamos el Form
        guardarNombre('');
        guardarCantidad(0);

    }

    return (
        <form
            onSubmit={agregarGasto}
        >
            <h2>Agrega tus gastos aquí</h2>
            {error ? <Error mensaje="Error al introducir los datos" /> : null }
            <div className="campo">
                <label>Nombre del gasto</label>
                <input
                    type="text"
                    className="u-full-width"
                    placeholder="Ej. Tranporte"
                    value={nombre}
                    onChange={e => guardarNombre(e.target.value)}
                />
            </div>

            <div className="campo">
                <label>Cantidad Gasto</label>
                <input
                    type="number"
                    className="u-full-width"
                    placeholder="Ej. 80"
                    value={cantidad}
                    onChange={e => guardarCantidad(parseInt(e.target.value))}
                />
            </div>
            <input
                type="submit"
                className="button-primary u-full-width"
                value="Agregar Gasto"
            />
        </form>

    );
}

Formulario.propTypes = {
    guardarGasto: PropTypes.func.isRequired,
    guardarCrearGasto: PropTypes.func.isRequired
}

export default Formulario;