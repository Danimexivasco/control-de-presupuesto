import React, { useState, useEffect } from 'react';
import Pregunta from './components/Pregunta';
import Formulario from './components/Formulario';
import Listado from './components/Listado';
import ControlPresupuesto from './components/ControlPresupuesto';


function App() {

  // State del Presupuesto
  const [presupuesto, guardarPresupuesto] = useState(0);

  // State del Restante
  const [restante, guardarRestante] = useState(0);

  // State para elegir que se muestra
  const [mostrarPregunta, actualizarPregunta] = useState(true);

  // State para gastos 
  const [gastos, guardarGastos] = useState([]);

  // State para gasto
  const [gasto, guardarGasto] = useState({});

  // State para crearGasto
  const [crearGasto, guardarCrearGasto] = useState(false);

  // useEffect que actualiza el restante
  useEffect(() => {
    if (crearGasto) {

      // agrega el nuevo prespuesto
      guardarGastos([
        ...gastos,
        gasto
      ])

      // resta del presupuesto actual
      const presupuestoRestante = restante - gasto.cantidad;
      guardarRestante(presupuestoRestante)

      // Reseteamos a false
      guardarCrearGasto(false)
    }
  }, [gasto, crearGasto, gastos, restante])

  // Cuando agregemos un nuveo gasto
  // const agregarNuevoGasto = gasto => {
  //   guardarGastos([
  //     ...gastos,
  //     gasto
  //   ])
  // }



  return (
    <div className="container">
      <header>
        <h1>Gasto Semanal</h1>
        <div className="contenido-principal contenido">
          {mostrarPregunta ?
            (<Pregunta
              guardarPresupuesto={guardarPresupuesto}
              guardarRestante={guardarRestante}
              actualizarPregunta={actualizarPregunta}
            />)
            :
            (<div className="row">
              <div className="one-half column">
                <Formulario
                  guardarGasto={guardarGasto}
                  guardarCrearGasto={guardarCrearGasto}
                // agregarNuevoGasto={agregarNuevoGasto}
                />
              </div>
              <div className="one-half column">
                <Listado
                  gastos={gastos}
                />
                <ControlPresupuesto
                  presupuesto={presupuesto}
                  restante={restante}
                />
              </div>
            </div>)
          }


        </div>
      </header>
    </div>
  );
}

export default App;
